#parameteres
#$1 - source 1 branch name
#$2 - source 2 branch name
#$3 - target branch name
origin_source1="origin/${1}"
origin_source2="origin/${2}"
origin_target="origin/${3}" 
#echo "Branch names" $merge_branch_name $origin_source $origin_target
git fetch --progress "--all"
git checkout -B ${3} ${origin_target}
git merge ${origin_source1}
git merge ${origin_source2}
git push --recurse-submodules=check --progress "origin" refs/heads/${3}:refs/heads/${3}