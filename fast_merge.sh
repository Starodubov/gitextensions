#parameteres
#$1 - source branch name
#$2 - target branch name
#$3 - target branch name prefix
if [ -z "$3" ]
  then branch_prefix=""
  else branch_prefix="${3}_"
fi

timestamp=`date +%Y%m%d_%H%M`
merge_branch_name="${timestamp}_${branch_prefix}merge_${1}_to_${2}"
merge_branch_name="$(tr '/-' '_' <<<$merge_branch_name)"
origin_source="${1}"
origin_target="origin/${2}" 
#echo "Branch names" $merge_branch_name $origin_source $origin_target
git fetch --progress "--all"
git checkout --no-track ${origin_target} -b ${merge_branch_name}
git merge ${origin_source}