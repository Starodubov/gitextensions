#parameteres
#$1 - master branch name
git checkout ${1}
git pull --progress "origin"
git merge origin/${1}
git remote prune origin
git branch --merged | grep -ivP "master|release|production|prod-fix.*|uat|uat-fix.*" | xargs git branch -d
git tag -l | xargs git tag -d && git fetch --tags