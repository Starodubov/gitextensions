#parameteres
#$1 - source branch name
#$2 - version
#$3 - branch prefix
origin_source="${1}" 
target_branch="${3}/${2}"

pattern="^origin\/.*"
if [[ ! $origin_source =~ $pattern ]]; then
	origin_source="origin/${origin_source}";
fi

#echo "Branch names" $origin_source $target_branch $pattern
git fetch --progress "--all"
git checkout --no-track ${origin_source} -b ${target_branch}
git push -u origin ${target_branch}